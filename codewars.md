

# about

[codewars](https://www.codewars.com/)是国外的一个代码刷题网站，简单来说，这是个代码闯关游戏，根据网站出的题目用代码实现，进而得分升级段位(初始段位是8kyu，数字越低段位越高)。

PS：注册时，要先选择一个语言，然后经过简单的测试，然后才能进行注册。

完事后，就可以刷题挑战升级段位了。

# 7kyu

## [Complementary DNA](https://www.codewars.com/kata/554e4a2f232cdd87d9000038)

脱氧核糖核酸(DNA)是一种在细胞核中发现的化学物质，为生物体的发育和功能提供“指令”。

在DNA串中，符号“A”和“T”是相互补充的，如“C”和“G”。你可以用DNA的一边(弦，除了Haskell);你需要得到互补的另一边。DNA链从来都不是空的，或者根本就没有DNA(同样，除了Haskell)。

示例：

```python
DNA_strand ("ATTGC") # return "TAACG"
DNA_strand ("GTAT") # return "CATA"
```

参考示例：

```python
# 自己实现，其它得分也是类似处理，可读性较强
def DNA_strand(dna):
    d = {"A": "T", "C": "G", "T": "A", "G": "C"}
    return ''.join([d[i] for i in dna])

# 得分最高，知识点有点偏门啊！
# Python3中直接执行
def DNA_strand(dna):
    return dna.translate(str.maketrans("ATCG","TAGC"))

print(DNA_strand("ATTGC"))  # TAACG
print(DNA_strand("AAAACCC"))  # TTTTGGG

# 思路是映射和替换
i = 'AT'
o = 'TA'
t = str.maketrans(i, o)
print(t)  # {65: 84, 84: 65}
print('AATT'.translate(t))  # TTAA
```



# 6kyu

## [Stop gninnipS My sdroW!](https://www.codewars.com/kata/5264d2b162488dc400000001)

编写一个函数，接受一个或多个单词组成的字符串，并返回相同的字符串，但所有五个或五个以上的字母单词都取反。

-   传入的字符串将仅由字母和空格组成。

-   只有在出现一个以上的单词时才会包含空格。

例如：

```python
spinWords("Hey fellow warriors") => "Hey wollef sroirraw" 
spinWords("This is a test") => "This is a test" 
spinWords("This is another test") => "This is rehtona test"
```

答案示例：

```python
# 自己实现的
def f1(sentence):
    res = ''
    if sentence:
        for i in sentence.split(' '):
            if i.__len__() >= 5:
                res += i[::-1] + ' '
            else:
                res += i + ' '
    return res.strip()

# 得分最高的
def spin_words(sentence):
    return " ".join([x[::-1] if len(x) >= 5 else x for x in sentence.split(" ")])
```

